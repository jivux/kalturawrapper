//
//  KalturaWrapperTests.swift
//  KalturaWrapperTests
//
//  Created by Ivo Reyes on 5/25/15.
//  Copyright (c) 2015 Triolabs. All rights reserved.
//

import UIKit
import XCTest
import KalturaWrapper
import SwiftyJSON

class BaseCase: XCTestCase {
    let timeout: NSTimeInterval = 10
    let loginId  = "it@triolabs.mx"
    let password = "Tr10.4dmin*"
    
    var session: KalturaSession?
}

class KalturaWrapperLoginTests: BaseCase {

    func testSuccessfulLogin() {
        // This is an example of a functional test case.
        var kSession: KalturaSession?
        let expectation = expectationWithDescription("Login")
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (aSession) in
            kSession = aSession
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertNotNil(kSession, "Didn't login")
    }
    
    func testUnsuccessfulLogin() {
        // This is an example of a functional test case.
        var kSession: KalturaSession?
        let expectation = expectationWithDescription("Login")
        
        KalturaSession.createSession(loginId: loginId, password: "asdfasdf") {
            (aSession) in
            kSession = aSession
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertNil(kSession, "Did login")
    }
    
    func testSuccessfulSessionFromKs() {
        let expectation = expectationWithDescription("Create session from ks")
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (kalturaSession) in
            KalturaSession.createSessionFromKs(ks: kalturaSession!.ks) {
                (aSession) in
                self.session = aSession
                expectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertNotNil(session, "Didn't retrieve session from login")
    }
    
    func testUnsuccessfulSessionFromKs() {
        let expectation = expectationWithDescription("Create session from ks")
        
        KalturaSession.createSessionFromKs(ks: "asdfasdflkadfadfafd") {
            (aSession) in
            self.session = aSession
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertNil(session, "Retrieve session when shouldn't")
    }
    
}

class KalturaWrapperCategoryTests: BaseCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testCategoryList() {
        let expectation = expectationWithDescription("Category List")
        var numberOfCategories: Int = 9
        var categories: [JSON] = [JSON]()
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getCategoryList() {
                (cats) in
                categories = cats
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(numberOfCategories, count(categories), "Didn't retrieve categories")
    }
    
    func testGetCategory() {
        let expectation = expectationWithDescription("Category")
        var category: JSON = [:]
        let categoryName = "Salud y estilo de vida"
        let referenceId  = "30350472"
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getCategory("30350472") {
                (cat) in
                category = cat
                expectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(categoryName, category["name"].stringValue, "Didn't get category")
    }
    
    func testGetPagedCategoryList() {
        let expectation = expectationWithDescription("Paged Category List")
        let pageSize = 3
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            let pagedRequest = session!.getPagedCategoryList(pageSize)
            XCTAssertNotNil(pagedRequest, "Didn't create paginated request")
            
            pagedRequest?.next() {
                (categories) in
                XCTAssertEqual(pageSize, count(categories), "Didn't retrieve the expected categories")
            }
            
            pagedRequest?.next() {
                (categories) in
                XCTAssertEqual(pageSize, count(categories), "Didn't retrieve the expected categories")
            }
            
            pagedRequest?.next() {
                (categories) in
                XCTAssertEqual(pageSize, count(categories), "Didn't retrieve the expected categories")
            }
            
            pagedRequest?.next() {
                (categories) in
                XCTAssertEqual(0, count(categories), "Didn't retrieve the expected categories")
            }
            
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }
}

class KalturaWrapperMediaTests: BaseCase {

    override func setUp() {
        super.setUp()
    }
    
    func testGetMedia() {
        let expectation = expectationWithDescription("Get Media with entry id")
        let entryId     = "0_13h7gbe2"
        let entryName   = "Calamardo Guapo"
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            session!.getMedia(entryId) {
                (entry) in
                XCTAssertEqual(entryName, entry["name"].stringValue, "Didn't fetch category")
                expectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }
    
    func testMediaList() {
        let expectation = expectationWithDescription("Media List")
        var numberOfMediaEntries: Int = 30
        var mediaEntries: [JSON] = [JSON]()
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getMediaList() {
                (entries) in
                mediaEntries = entries
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(numberOfMediaEntries, count(mediaEntries), "Didn't retrieve media entries")
    }
    
    func testMediaListOfCategories() {
        let expectation = expectationWithDescription("Media List of Categories")
        let categories = ["30350472", "30402232"]
        var numberOfMediaEntries: Int = 15
        var mediaEntries: [JSON] = [JSON]()
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getMediaListOfCategories(categories) {
                (entries) in
                mediaEntries = entries
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(numberOfMediaEntries, count(mediaEntries), "Didn't retrieve media entries")
    }
    
    func testMediaListOfCategoriesAndType() {
        let expectation = expectationWithDescription("Media List of Categories and Type")
        let categories = ["30350472", "30402232"]
        let mediaType = MediaType.Video
        var numberOfMediaEntries: Int = 6
        var mediaEntries: [JSON] = [JSON]()
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getMediaListOfCategoriesAndType(categories, mediaType: mediaType) {
                (entries) in
                mediaEntries = entries
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(numberOfMediaEntries, count(mediaEntries), "Didn't retrieve media entries")
    }
    
    func testMediaListWithTags() {
        let expectation = expectationWithDescription("Media List with Tags")
        let tags = ["cartoon", "anime"]
        var numberOfMediaEntries: Int = 8
        var mediaEntries: [JSON] = [JSON]()
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getMediaWithTags(tags) {
                (entries) in
                mediaEntries = entries
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(numberOfMediaEntries, count(mediaEntries), "Didn't retrieve media entries")
    }
}

class KalturaWrapperAttachmentAssetTests: BaseCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testAttachmentAssetList() {
        let expectation = expectationWithDescription("Attachment Asset List")
        let filename = "RC452_Estudio-centros-de-lavado.pdf"
        var attachmentAssets: [JSON] = [JSON]()
        let entryId = "0_1qe7z15n"
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getAttachmentAssetList(entryId) {
                (entries) in
                attachmentAssets = entries
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(filename, attachmentAssets[0]["filename"].stringValue, "Didn't retrieve attachment asset")
    }
    
    func testGetAttachmentAssetUrl() {
        let expectation = expectationWithDescription("Attachment Asset URL")
        var attachmentAssetUrl = ""
        let id = "0_ka99141h"
        
        KalturaSession.createSession(loginId: loginId, password: password) {
            (session) in
            
            session!.getAttachmentAssetUrl(id) {
                (url) in
                attachmentAssetUrl = url
                expectation.fulfill()
            }
        }
        
        
        waitForExpectationsWithTimeout(timeout, handler: nil)

        XCTAssertNotEqual("", attachmentAssetUrl, "Didn't retrieve attachment asset url")
    }
}
