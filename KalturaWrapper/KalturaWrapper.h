//
//  KalturaWrapper.h
//  KalturaWrapper
//
//  Created by Ivo Reyes on 5/25/15.
//  Copyright (c) 2015 Triolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KalturaWrapper.
FOUNDATION_EXPORT double KalturaWrapperVersionNumber;

//! Project version string for KalturaWrapper.
FOUNDATION_EXPORT const unsigned char KalturaWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KalturaWrapper/PublicHeader.h>


