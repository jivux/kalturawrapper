//
//  KalturaWrapper.swift
//  KalturaWrapper
//
//  Created by Ivo Reyes on 5/25/15.
//  Copyright (c) 2015 Triolabs. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/**
Supported media types in kaltura.
**/
public enum MediaType: Int
{
    case Image = 2
    case Audio = 5
    case Video = 1
}


public enum MetadataObjectType: Int
{
    case Entry          = 1
    case Category       = 2
    case User           = 3
    case Partner        = 4
    case DynamicObject  = 5
}


private struct UrlConstants
{
    static let URL = "http://www.kaltura.com/api_v3/"

    static let format = "1"
    
    // Not exactly an error flag, but all the error responses contain this field.
    static let errorFlag = "code"

    static let serviceParam  = "service"
    static let actionParam   = "action"
    static let ksParam       = "ks"
    static let idParam       = "id"
    static let entryIdParam  = "entryId"
    static let formatParam   = "format"
    static let loginIdParam  = "loginId"
    static let passwordParam = "password"
    
    static let userService              = "user"
    static let mediaService             = "media"
    static let sessionService           = "session"
    static let categoryService          = "category"
    static let metadataService          = "metadata_metadata"
    static let attachmentAssetService   = "attachment_attachmentasset"
    
    static let getAction    = "get"
    static let listAction   = "list"
    static let getUrlAction = "getUrl"
    static let loginAction  = "loginByLoginId"
    
    static let entryIdEqualFilter               = "filter:entryIdEqual"
    static let categoriesIdsMatchOrFilter       = "filter:categoriesIdsMatchOr"
    static let mediaTypeEqualFilter             = "filter:mediaTypeEqual"
    static let tagsFilter                       = "filter:tagsNameMultiLikeOr"
    static let metadataObjectTypeEqualFilter    = "filter:metadataObjectTypeEqual"
    
    static let pageSizePager    = "pager:pageSize"
    static let pageIndexPager   = "pager:pageIndex"
}


/**
Iterator for a paged request.
**/
public class KalturaPagedRequest
{
    public private(set) var currentPageIndex: Int = 0
    public private(set) var pageSize: Int = 0
    private var params = [String : String]()
    
    /**
    Returns the next batch of entries. Each batch will be equal to the page size in length, except in the last entry if the are less left. When it returns an empty list, it means the iterator has ended.
    :param: callback the function to be executed in this iteration.
    **/
    public func next(callback: ([JSON]) -> Void) {
        currentPageIndex++
        self.params[UrlConstants.pageIndexPager] = "\(currentPageIndex)"
        RestClient.getList(params, callback: callback)
    }
    
    /**
    :returns: If the page size is between 1 and 500, inclusive, it will return an instance of KalturaPagedRequest; else, nil.
    **/
    public class func createPagedRequest(params: [String : String], pageSize: Int) -> KalturaPagedRequest? {
        if pageSize < 1 || pageSize > 500 {
            return nil
        } else {
            let pagedRequest = KalturaPagedRequest()
            pagedRequest.pageSize = pageSize
            pagedRequest.params = params
            pagedRequest.params[UrlConstants.pageSizePager] = "\(pagedRequest.pageSize)"
            return pagedRequest
        }
    }
}


private class RestClient
{
    class func getList(params: [String : String], callback: ([JSON]) -> Void) {
        
        Alamofire.request(.GET, UrlConstants.URL, parameters: params).responseJSON {
            (_, _, json, _) in
            let swiftyJson = JSON(json!)
            
            if let error = swiftyJson[UrlConstants.errorFlag].string {
                callback([])
            } else {
                callback(swiftyJson["objects"].arrayValue)
            }
        }
    }
    
    class func getObject(params: [String : String], callback: (JSON) -> Void) {
        Alamofire.request(.GET, UrlConstants.URL, parameters: params).responseJSON {
            (_, _, json, _) in
            let swiftyJson = JSON(json!)
            
            if let error = swiftyJson[UrlConstants.errorFlag].string {
                callback([:])
            } else {
                callback(swiftyJson)
            }
        }
    }
    
    class func getString(params: [String : String], callback: (String) -> Void) {
        Alamofire.request(.GET, UrlConstants.URL, parameters: params).responseJSON {
            (_, _, json, _) in
            let swiftyJson = JSON(json!)
            
            if let error = swiftyJson[UrlConstants.errorFlag].string {
                callback("")
            } else {
                callback(swiftyJson.stringValue)
            }
        }
    }
    
    class func login(params: [String : String], callback: (KalturaSession?) -> Void) {
        Alamofire.request(.GET, UrlConstants.URL, parameters: params).responseJSON {
            (_, _, json, _) in
            if let res = json as? String {
                // In the Kaltura API a successful login just returns the ks string.
                let session = KalturaSession()
                session.ks = json as! String
                
                callback(session)
            } else {
                callback(nil)
            }
        }
    }
    
    class func getSession(ks: String, params: [String : String], callback: (KalturaSession?) -> Void) {
        Alamofire.request(.GET, UrlConstants.URL, parameters: params).responseJSON {
            (_, _, json, _) in
            let swiftyJson = JSON(json!)

            if let error = swiftyJson[UrlConstants.errorFlag].string {
                callback(nil)
            } else {
                let session = KalturaSession()
                session.ks = ks
                callback(session)
            }
        }
    }
    
}

/**
    Responsible for making all the requests to Kaltura.
*/
public class KalturaSession
{
    public private(set) var ks: String = "" {
        didSet {
            ks = ks.stringByReplacingOccurrencesOfString("\"", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        }
    }

    
    // MARK: - Login Services
    
    /**
    Login to Kaltura. If the login was successful it will return a KalturaSession; else, nil.
    
    :param: loginId Kaltura username.
    :param: password Kaltura password.
    :param: callback function to be executed.
    **/
    public class func createSession(#loginId: String, password: String, callback: (KalturaSession?) -> Void) {
        let params = formLoginParameters(loginId: loginId, withPassword: password)
        RestClient.login(params, callback: callback)
    }
    
    /**
    Creates a KalturaSession instance if given a valid ks; else returns nil.
    
    :param: ks session string to Kaltura.
    :param: callback function to be executed.
    **/
    public class func createSessionFromKs(#ks: String, callback: (KalturaSession?) -> Void) {
        let params = formGetSessionParameters(ks: ks)
        RestClient.getSession(ks, params: params, callback: callback)
    }
    
    
    // MARK: Media Services
    
    
    /**
    Gets the media entry with the given Id. If there are no entries with such id, it returns an empty JSON object.
    :param: entryId the media entry id to search.
    :param: callback function to be executed
    **/
    public func getMedia(entryId: String, callback: (JSON) -> Void) {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.getAction)
        params[UrlConstants.entryIdParam] = entryId
        RestClient.getObject(params, callback: callback)
    }

    /**
        Provides a list of all the media entries ordered chronologically. In case of error, it will return an empty list.

        :param: callback function to be executed.
    */
    public func getMediaList(callback: ([JSON]) -> Void) {
        let params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        RestClient.getList(params, callback: callback)
    }
    
    /**
    Provides a paginated list of all the media entries ordered chronologically.
    
    :param: pageSize the number of media entries per page, should be between 1 and 500
    :returns: An instance of KalturaPagedRequest if there are no errors; else, nil.
    */
    public func getPagedMediaList(pageSize: Int) -> KalturaPagedRequest? {
        let params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        return KalturaPagedRequest.createPagedRequest(params, pageSize: pageSize)
    }
    
    /**
        Provides a list of all the media entries, ordered chronologically, that belong to one or more of the categories specified. In case of error, it will return an empty list.
    
        :param: categoriesId list of categories ids.
        :param: callback function to be executed.
    */
    public func getMediaListOfCategories(categoriesId: [String], callback: ([JSON]) -> Void) {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.categoriesIdsMatchOrFilter] = join(",", categoriesId)
        RestClient.getList(params, callback: callback)
    }
    
    /**
        Provides a paginated list of all the media entries, ordered chronologically, that belong to one or more of the categories specified.
    
        :param: categoriesId list of categories ids.
        :param: pageSize the number of media entries per page, should be between 1 and 500
    */
    public func getPagedMediaListOfCategories(categoriesId: [String], pageSize: Int) -> KalturaPagedRequest? {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.categoriesIdsMatchOrFilter] = join(",", categoriesId)
        return KalturaPagedRequest.createPagedRequest(params, pageSize: pageSize)
    }
    
    /**
        Provides a list of all the media entries, ordered chronologically, that contains one or more of the tags specified. In case of error, it will return an empty list.
    
        :param: tags list of tags
        :param: callback function to be executed.
    */
    public func getMediaWithTags(tags: [String], callback: ([JSON]) -> Void) {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.tagsFilter] = join(",", tags)
        RestClient.getList(params, callback: callback)
    }
    
    /**
    Provides a paged list of all the media entries, ordered chronologically, that contains one or more of the tags specified.
    
    :param: tags list of tags
    :param: pageSize the number of media entries per page, should be between 1 and 500
    */
    public func getPagedMediaWithTags(tags: [String], pageSize: Int) -> KalturaPagedRequest? {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.tagsFilter] = join(",", tags)
        return KalturaPagedRequest.createPagedRequest(params, pageSize: pageSize)
    }
    
    /**
        Provides a list of all the media entries, ordered chronologically, that belong to one or more of the specified categories and are of the selected media type. In case of error, it will return an empty list.
    
        :param: categoriesId list of categories ids.
        :param: mediaType desired type of media.
        :param: callback function to be executed.
    */
    public func getMediaListOfCategoriesAndType(categoriesId: [String], mediaType: MediaType, callback: ([JSON]) -> Void) {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.categoriesIdsMatchOrFilter] = join(",", categoriesId)
        params[UrlConstants.mediaTypeEqualFilter] = "\(mediaType.rawValue)"
        RestClient.getList(params, callback: callback)
    }
    
    /**
    Provides a paged list of all the media entries, ordered chronologically, that belong to one or more of the specified categories and are of the selected media type.
    
    :param: categoriesId list of categories ids.
    :param: mediaType desired type of media.
    :param: pageSize the number of media entries per page, should be between 1 and 500
    **/
    public func getPagedMediaListOfCategoriesAndType(categoriesId: [String], mediaType: MediaType, pageSize: Int) -> KalturaPagedRequest? {
        var params = formParameters(UrlConstants.mediaService, action: UrlConstants.listAction)
        params[UrlConstants.categoriesIdsMatchOrFilter] = join(",", categoriesId)
        params[UrlConstants.mediaTypeEqualFilter] = "\(mediaType.rawValue)"
        return KalturaPagedRequest.createPagedRequest(params, pageSize: pageSize)
    }
    
    // MARK: Category Services

    /**
        Provides the category with the specified referenceId. In case of error or non-found referenceId, it will return an empty json object.
    
        :param: referenceId id of the category.
        :param: callback function to be executed.
    */
    public func getCategory(referenceId: String, callback: (JSON) -> Void) {
        var params = formParameters(UrlConstants.categoryService, action: UrlConstants.getAction)
        params[UrlConstants.idParam] = referenceId
        
        RestClient.getObject(params, callback: callback)
    }

    /**
    Provides a list with all the categories. In case of error, it will return an empty list.
    :param: callback function to be executed.
    **/
    public func getCategoryList(callback: ([JSON]) -> Void) {
        let params = formParameters(UrlConstants.categoryService, action: UrlConstants.listAction)
        RestClient.getList(params, callback: callback)
    }
    
    /**
    Creates a paged request of all the categories.
    :param: pageSize the number of categories per page, should be between 1 and 500
    :returns: An instance of KalturaPagedRequest if there are no errors; else, nil.
    **/
    public func getPagedCategoryList(pageSize: Int) -> KalturaPagedRequest? {
        let params = formParameters(UrlConstants.categoryService, action: UrlConstants.listAction)
        return KalturaPagedRequest.createPagedRequest(params, pageSize: pageSize)
    }
    
    
    // MARK: Metadata Services
    
    /**
    Returns a list of all the metadata for each entry of the selected type.
    :param: objectType the kind of objects you want their metadata
    :param: callback function to be executed.
    **/
    public func getMetadataForObjectsOfType(objectType: MetadataObjectType, callback: ([JSON]) -> Void) {
        var params = formParameters(UrlConstants.metadataService, action: UrlConstants.listAction)
        params[UrlConstants.metadataObjectTypeEqualFilter] = "\(objectType.rawValue)"
        RestClient.getList(params, callback: callback)
    }
    
    // MARK: Attachment Assets Services

    /**
    Provides a list with all the attachments assets of an entry. In case of error or non-found entry, it will return an empty list.
    
    :param: entryId id of the entry.
    :param: callback function to be executed.
    **/
    public func getAttachmentAssetList(entryId: String, callback: ([JSON]) -> Void) {
        var params = formParameters(UrlConstants.attachmentAssetService, action: UrlConstants.listAction)
        params[UrlConstants.entryIdEqualFilter] = entryId
        
        RestClient.getList(params, callback: callback)
    }

    /**
    Fetchs the URL of the resource of the selected attachment asset. In case of error, it will return an empty string.
    
    :param: attachmentAssetId id of the attachment asset.
    :param: callback function to be executed.
    **/
    public func getAttachmentAssetUrl(attachmentAssetId: String, callback: (String) -> Void) {
        var params = formParameters(UrlConstants.attachmentAssetService, action: UrlConstants.getUrlAction)
        params[UrlConstants.idParam] = attachmentAssetId
        RestClient.getString(params, callback: callback)
    }
}

private extension KalturaSession
{

    private func formParameters(service: String, action: String) -> [String:String] {
        return [UrlConstants.serviceParam: service, UrlConstants.actionParam: action,
            UrlConstants.ksParam: self.ks, UrlConstants.formatParam: UrlConstants.format]
    }

    private class func formLoginParameters(#loginId: String, withPassword password: String) -> [String:String] {
        return [UrlConstants.loginIdParam: loginId, UrlConstants.passwordParam: password,
            UrlConstants.formatParam: UrlConstants.format, UrlConstants.serviceParam:UrlConstants.userService,
            UrlConstants.actionParam: UrlConstants.loginAction]
    }
    
    private class func formGetSessionParameters(#ks: String) -> [String : String] {
        return [UrlConstants.serviceParam: UrlConstants.sessionService,
            UrlConstants.actionParam: UrlConstants.getAction, UrlConstants.ksParam: ks,
            UrlConstants.formatParam: UrlConstants.format]
    }
}
